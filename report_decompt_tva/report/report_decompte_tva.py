# -*- encoding: utf-8 -*-

from openerp.addons.jasper_reports import jasper_report
import openerp.pooler
import time, datetime
import base64
import os


def generate_records( cr, uid, ids, data,context):
        #pool= pooler.get_pool(cr.dbname)

        if 'form' in data:
            from_date = data['form']['date_begin']
            to_date = data['form']['date_end']     
            data={
                    'date_begin':from_date,
                    'date_end':to_date,
                      }
        return data

jasper_report.report_jasper('report.jasper_report_decompte_tva',model='account.invoice',parser=generate_records)

  

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
